import screeninfo


def get_main_screen_dimensions():
    screens = screeninfo.get_monitors()
    for screen in screens:
        if not screen.is_primary:
            continue
        return screen.width, screen.height

width, height = get_main_screen_dimensions()
print(f"Width: {width}, height: {height}")
