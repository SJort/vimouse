# input = [1, 2, 5]
# order = [3, 2, 4, 5, 1]
#
# output = []
# for x in order:
#     if x in input:
#         output.append(x)
#
# print("Before:", input)
# print("After:", output)


data = [1, 2, 3, 4, 5, 6, 8, 9, 32, 33, 34]
number = 33

output = []

for x in range(0, len(data)):
    output.append(data[x])
    try:
        if data[x] + 1 != data[x + 1]:
            if number in output:
                break
            else:
                output.clear()
    except IndexError:
        print("Not found")

print("Input:", data)
print("Output:", output)

shortcut = "oi"
if "hoi".find(shortcut, 0, len(shortcut)) != -1:
    print("hehe")
