def test(text):
    print("Text:", text)


def test2(text, haha="meui"):
    print("Text:", text, "haha:", haha)


def caller(function, *args, **kwargs):
    function(args, kwargs)


caller(test2, "haha", haha="lol")


def method1(spam, ham):
    return 'hello ' + str(spam) + ' and ' + str(ham)


def method2(methodToRun, positional_arguments, keyword_arguments):
    return methodToRun(*positional_arguments, **keyword_arguments)


print("StackOverflow:", method2(method1, ['spam'], {'ham': 'ham'}))


def perform(f):
    f()


def go(haha, hoi):
    print("haha:", haha, "hoi:", hoi)


perform(lambda: go("hoi", "haha"))
