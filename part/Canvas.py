from math import gcd
from tkinter import Tk, Canvas


class GridItem:
    def __init__(self, size, x, y):
        self.size = size
        self.x = x
        self.y = y


class Grid:
    def __init__(self, root, common_factor, width, height):
        self.canvas = Canvas(root, width=width, height=height, highlightthickness=0)
        self.amount_of_columns = int(width / common_factor)
        self.amount_of_rows = int(height / common_factor)
        self.grid_items = []
        self.drawn = False
        for x in range(0, self.amount_of_columns):
            for y in range(0, self.amount_of_rows):
                # print("Iterate")
                x_grid = x * common_factor
                y_grid = y * common_factor
                item = GridItem(common_factor, x_grid, y_grid)
                self.grid_items.append(item)

    def show(self):
        if self.drawn:
            self.canvas.pack()
            return
        for grid_item in self.grid_items:
            x = grid_item.x
            y = grid_item.y
            size = grid_item.size
            # print("x:", x, "y:", y, "size:", size)
            self.canvas.create_rectangle(x, y, x + size, y + size)
        self.canvas.pack()
        self.drawn = True

    def hide(self):
        self.canvas.pack_forget()


def common_factors(x, y, min):
    result = []
    gdc = gcd(x, y)
    for x in range(1, gdc):
        if gdc % x == 0:
            if min is not None:
                if x < min:
                    continue
            result.append(x)

    return result


class GridManager:

    def __init__(self):
        self.root = Tk()
        self.root.overrideredirect(True)  # disable icon
        self.window_width = self.root.winfo_screenwidth()
        self.window_height = self.root.winfo_screenheight()
        self.root.geometry("{0}x{1}+0+0".format(self.window_width, self.window_height))
        self.root.attributes('-transparentcolor', self.root['bg'])

        # possible factors of 1920x1080: 1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60, 120
        # https://www.calculatorsoup.com/calculators/math/commonfactors.php
        common_factor = 40
        self.grids = []
        factors = common_factors(self.window_height, self.window_width, 10)
        for factor in factors:
            print("Iterating factor", factor)
            grid = Grid(self.root, factor, self.window_width, self.window_height)
            self.grids.append(grid)

        self.active_grid = self.grids[0]
        self.active_grid.show()

        self.index = 0

        self.root.bind("<c>", lambda x: self.switch())
        self.root.bind("<Escape>", lambda x: self.root.destroy())
        self.root.mainloop()

    def switch(self):
        print("Switch")
        self.active_grid.hide()
        self.index += 1
        if self.index >= len(self.grids):
            self.index = 0
        self.active_grid = self.grids[self.index]
        self.active_grid.show()


GridManager()
