from tkinter import Tk

from pynput import keyboard
from pynput.keyboard import Key


class Shortcut:
    def __init__(self, combination, listener):
        self.combination = combination
        self.listener = listener


class ShortcutHandler:
    def __init__(self):
        self.listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
        self.listener.start()
        self.match = False
        self.pressed_shortcut_keys = []
        self.shortcuts = []

    """
    Combination: a list with keys to listen to, use 'Key.cmd' etc for special keys and a string for normal keys like 'a'
    Listener: a function which should be called when shortcut is pressed
    Idea: pass optional **kwargs which should be given with the function
    """

    def add_shortcut(self, combination, listener):
        shortcut = Shortcut(combination, listener)
        self.shortcuts.append(shortcut)

    def on_press(self, key):
        to_add = key

        if hasattr(key, "char"):
            to_add = key.char  # append the char instead of the key

        if to_add not in self.pressed_shortcut_keys:
            self.pressed_shortcut_keys.append(to_add)

        for shortcut in self.shortcuts:
            if shortcut.combination == self.pressed_shortcut_keys:
                self.match = True

    def on_release(self, key):
        if self.match:
            for shortcut in self.shortcuts:
                if shortcut.combination == self.pressed_shortcut_keys:
                    shortcut.listener()
        self.pressed_shortcut_keys.clear()


def on_shortcut():
    print("Shortcut pressed!")

def on_shortcut2():
    print("Second shortcut!")


handler = ShortcutHandler()
handler.add_shortcut([Key.cmd, "c"], on_shortcut)
handler.add_shortcut([Key.cmd, "c"], on_shortcut2)
root = Tk()
root.bind('<Escape>', lambda x: root.destroy())
root.mainloop()
