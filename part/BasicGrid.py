from tkinter import Tk, Canvas

root = Tk()
root.overrideredirect(True)  # disable icon
window_width = root.winfo_screenwidth()
window_height = root.winfo_screenheight()
root.geometry("{0}x{1}+0+0".format(window_width, window_height))
# root.attributes('-transparentcolor', root['bg'])
root.wm_attributes('-alpha', 0.4)

# possible factors of 1920x1080: 1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60, 120
# https://www.calculatorsoup.com/calculators/math/commonfactors.php
common_factor = 40
rows_amount = window_height / common_factor
rows_amount = int(rows_amount)
columns_amount = window_width / common_factor
columns_amount = int(columns_amount)


class GridItem:
    def __init__(self, size, x, y):
        self.size = size
        self.x = x
        self.y = y

grid_items = []
for x in range(0, columns_amount):
    for y in range(0, rows_amount):
        x_grid = x * common_factor
        y_grid = y * common_factor
        item = GridItem(common_factor, x_grid, y_grid)
        grid_items.append(item)

canvas = Canvas(root, width=window_width, height=window_height, highlightthickness=0)
canvas.create_line(0, 0, 1000, 1000)
for grid_item in grid_items:
    x = grid_item.x
    y = grid_item.y
    size = grid_item.size
    # print("x:", x, "y:", y, "size:", size)
    canvas.create_rectangle(x, y, x + size, y + size)





canvas.pack()


root.bind('<Escape>', lambda x: root.destroy())
root.mainloop()
