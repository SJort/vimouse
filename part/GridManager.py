from math import gcd
from tkinter import Tk, Canvas
import tkinter.font


class GridItem:
    def __init__(self, size, x, y, code):
        self.size = size
        self.x = x
        self.y = y
        self.code = code


class Grid:
    def __init__(self, root, common_factor, width, height):
        self.canvas = Canvas(root, width=width, height=height, highlightthickness=0)
        self.amount_of_columns = int(width / common_factor)
        self.amount_of_rows = int(height / common_factor)
        self.grid_items = []
        self.drawn = False
        self.font_size = int(common_factor / 4)
        self.font = tkinter.font.Font(family="Helvetica", size=self.font_size, weight="bold")
        for x in range(0, self.amount_of_columns):
            for y in range(0, self.amount_of_rows):
                # print("Iterate")
                x_grid = x * common_factor
                y_grid = y * common_factor
                item = GridItem(common_factor, x_grid, y_grid, "uWu")
                self.grid_items.append(item)

    def show(self):
        if self.drawn:
            self.canvas.pack()
            return
        for grid_item in self.grid_items:
            size = grid_item.size
            x = grid_item.x
            y = grid_item.y

            # center coordinates
            xc = x + size / 2
            yc = y + size / 2

            self.canvas.create_rectangle(x, y, x + size, y + size)
            if self.font_size > 1:
                self.canvas.create_text(xc, y + size - self.font_size,
                                        text=grid_item.code,
                                        # fill="",
                                        font=self.font,
                                        tag=(grid_item.code, "GridItem"),
                                        )

        self.canvas.pack()
        self.drawn = True

    def hide(self):
        self.canvas.pack_forget()


# possible factors of 1920x1080: 1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60, 120
# https://www.calculatorsoup.com/calculators/math/commonfactors.php
def common_factors(x, y, min):
    result = []
    gdc = gcd(x, y)
    for x in range(1, gdc):
        if gdc % x == 0:
            if min is not None:
                if x < min:
                    continue
            result.append(x)

    return result


class GridManager:

    def __init__(self, root):
        self.root = root
        self.window_width = self.root.winfo_screenwidth()
        self.window_height = self.root.winfo_screenheight()
        self.grids = []
        factors = common_factors(self.window_height, self.window_width, 10)
        for factor in factors:
            print("Iterating factor", factor)
            grid = Grid(self.root, factor, self.window_width, self.window_height)
            self.grids.append(grid)

        self.active_grid = self.grids[0]
        self.active_grid.show()

        self.index = 0


    def switch(self):
        print("Switch")
        self.active_grid.hide()
        self.index += 1
        if self.index >= len(self.grids):
            self.index = 0
        self.active_grid = self.grids[self.index]
        self.active_grid.show()


GridManager()
