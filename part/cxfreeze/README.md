## Installation
Install cx_Freeze
```shell
pip install cx_Freeze
```
Generate setup.py file
```shell
cxfreeze-quickstart
```
Build .exe with installer
```shell
python setup.py bdist_msi
```

Didn't this need an older Python version?
