from tkinter import Tk

from pynput import keyboard
from pynput.keyboard import Key

match = False
showing = True
pressed_shortcut_keys = []
toggle_shortcut_keys = [Key.cmd, 'c']


def on_press(key):
    global match

    to_add = key

    if hasattr(key, "char"):
        to_add = key.char
        pressed_shortcut_keys.append(key.char)  # append the char instead of the key

    if to_add not in pressed_shortcut_keys:
        pressed_shortcut_keys.append(to_add)

    if pressed_shortcut_keys == toggle_shortcut_keys:
        match = True


def on_release(key):
    global match
    pressed_shortcut_keys.clear()
    if match:
        match = False
        on_toggle_shortcut()


def on_toggle_shortcut():
    print("Match")
    global root
    global showing
    if showing:
        root.iconify()
    else:
        root.deiconify()

    showing = not showing


listener = keyboard.Listener(on_press=on_press, on_release=on_release)
listener.start()

root = Tk()
root.bind('<Escape>', lambda x: root.destroy())
root.mainloop()
