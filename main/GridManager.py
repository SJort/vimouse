import pyautogui

from main.Config import *
from main.Grid import Grid
from main.Util import *


# class to handle all the Grids of certain resolutions
class GridManager:
    def __init__(self, root):
        pyautogui.FAILSAFE = False  # disable delay in mouse control: max every 100 ms and move to corner to disable
        self.root = root
        self.grids = []

        # determine the common factors of the screen resolution so GridItem sizes do not have to be rounded
        factors = common_factors(window_height, window_width, 40)

        # create Grids for each factor
        for factor in factors:
            # each Grid needs to know the screen size so the canvas can be the same dimensions
            # factor = 40  # TODO TEMP
            grid = Grid(self.root, factor, window_width, window_height)
            self.grids.append(grid)

        print(f"Generated {len(self.grids)}")

        # load the first grid
        self.index = 0
        self.active_grid = self.grids[self.index]
        self.active_grid.show()

    def switch(self, up=True):

        # unpack the active grid
        self.active_grid.clear_input()
        self.active_grid.hide()

        if up:
            print("Decreasing grid resolution")
            self.index += 1
        else:
            print("Increasing grid resolution")
            self.index -= 1

        # go to beginning of list if at end
        if self.index >= len(self.grids):
            self.index = 0
        if self.index < 0:
            self.index = len(self.grids) - 1

        # pack the new grid
        self.active_grid = self.grids[self.index]
        self.active_grid.show()
