import tkinter
from tkinter import Canvas
from tkinter.font import Font

import pyautogui

from main.GridItem import GridItem
from main.KeyCodes import *
from main.Util import *

"""
one grid of a certain resolution, consists of multiple GridItems
managed by GridManager
"""


class Grid:
    def __init__(self, root, common_factor, width, height):
        # a separate canvas object on which the grid is drawn on
        self.canvas = Canvas(root, width=width, height=height, highlightthickness=0)

        # make int because the loops require it. no info is lost because of common factors
        self.amount_of_columns = int(width / common_factor)
        self.amount_of_rows = int(height / common_factor)

        self.amount_of_grid_items = self.amount_of_columns * self.amount_of_rows

        self.shortcut_start_count = get_minimal_length(self.amount_of_grid_items)  # don't start at 0, see function
        self.shortcut_length = len(get_key_code(self.shortcut_start_count))  # should be 3 or something
        self.common_factor = common_factor

        self.inputs = []  # keys typed for box shortcuts

        # in which column and row of griditems is the active griditem
        self.active_grid_column = 0
        self.active_grid_row = 0
        self.grid_items = [[]]
        self.drawn = False

        # custom font size to fit in every box
        self.font_size = int(common_factor / 4)
        self.font = Font(family="Helvetica", size=self.font_size, weight="bold")

        # create the GridItems for this grid
        count = get_minimal_length(self.amount_of_grid_items)  # don't start at 0, see function
        for y in range(0, self.amount_of_rows):
            self.grid_items.append([])  # append a new row
            for x in range(0, self.amount_of_columns):
                # fill the row with grid items
                x_grid = x * common_factor
                y_grid = y * common_factor
                item = GridItem(common_factor, x_grid, y_grid, get_key_code(count))
                self.grid_items[y].append(item)
                count += 1
        print("Created grid array:", len(self.grid_items), "and rows: ", len(self.grid_items[1]))

    def draw_grid_item(self, grid_item):
        # create the box
        self.canvas.create_rectangle(
            grid_item.x, grid_item.y,  # top left
            grid_item.x + grid_item.size, grid_item.y + grid_item.size,  # bottom right
            tag=(grid_item.shortcut, "GridItem"),
        )

        # create shortcut text on bottom of box if font readable
        if self.font_size > 1:
            self.canvas.create_text(grid_item.center_x, grid_item.y + grid_item.size - self.font_size,
                                    text=grid_item.shortcut,
                                    # fill="",
                                    font=self.font,
                                    # put the shortcut text as tag so GridItems can be hidden if
                                    # not matching with the typed shortcut. Also give each GridItem the same
                                    # tag so they all can be hidden simultaneously
                                    tag=(grid_item.shortcut, "GridItem"),
                                    )

    # set the mouse position in the grid as close to the real time mouse location
    def set_mouse_to_closest_box(self):
        print("Setting mouse to nearest GridItem...")
        mouse_x, mouse_y = pyautogui.position()
        self.active_grid_column = int(mouse_x / self.common_factor)
        self.active_grid_row = int(mouse_y / self.common_factor)

        try:
            # throws error when mouse is on other screen with higher resolution
            pyautogui.moveTo(self.get_active_grid_item().center_x, self.get_active_grid_item().center_y)
        except:
            print("Can't determine closest, defaulting...")
            self.active_grid_row = 0
            self.active_grid_column = 0
            pyautogui.moveTo(self.get_active_grid_item().center_x, self.get_active_grid_item().center_y)

    def move_mouse(self, direction, blocks):
        print("Moving mouse", direction)
        mouse_x, mouse_y = pyautogui.position()

        # snap the mouse to closest box if it has shifted
        # if difference with original position is not equal to movement, recenter (lisa)
        supposed_mouse_x = self.get_active_grid_item().center_x
        supposed_mouse_y = self.get_active_grid_item().center_y
        if mouse_x != supposed_mouse_x or mouse_y != supposed_mouse_y:
            print("Mouse shifted: sup_x=", supposed_mouse_x, "x=", mouse_x)
            print("Mouse shifted: sup_y=", supposed_mouse_y, "y=", mouse_y)
            self.set_mouse_to_closest_box()

        if direction == Direction.LEFT:
            self.active_grid_column -= blocks
        elif direction == Direction.DOWN:
            self.active_grid_row += blocks
        elif direction == Direction.UP:
            self.active_grid_row -= blocks
        elif direction == Direction.RIGHT:
            self.active_grid_column += blocks

        mouse_x = self.get_active_grid_item().center_x
        mouse_y = self.get_active_grid_item().center_y

        pyautogui.moveTo(mouse_x, mouse_y)

    def show(self):
        # if the canvas is already painted, just pack it
        if self.drawn:
            print("Showing pre-drawn grid...")
            self.canvas.pack()
        else:
            print("Drawing grid...")
            # if canvas is not painted yet, paint each GridItem
            for x in range(0, self.amount_of_columns):
                for y in range(0, self.amount_of_rows):
                    grid_item = self.grid_items[y][x]
                    self.draw_grid_item(grid_item)

                self.canvas.pack()
                # set drawn to true so the loop does not have to be executed again
                self.drawn = True

        # self.set_mouse_to_closest_box()  # todo: this makes it slow, maybe only do it when moving mouse?

    def get_active_grid_item(self):
        return self.grid_items[self.active_grid_row][self.active_grid_column]

    def hide(self):
        # remove the drawn canvas, but it keeps in memory
        self.canvas.pack_forget()

    # this function is fucked, how do i fix this...?
    def draw_grid_containing_shortcut(self, shortcut):
        # clear whole canvas
        self.canvas.delete("GridItem")
        shortcut = shortcut.upper()
        for y in range(0, self.amount_of_rows):
            for x in range(0, self.amount_of_columns):
                grid_item = self.grid_items[y][x]
                if grid_item.shortcut.startswith(shortcut):
                    self.draw_grid_item(grid_item)

    def get_grid_item_with_shortcut(self, shortcut):
        for y in range(0, self.amount_of_rows):
            for x in range(0, self.amount_of_columns):
                grid_item = self.grid_items[y][x]
                if grid_item.shortcut == shortcut:
                    return grid_item

    def on_shortcut_input(self, typed_key):
        print("Input: " + str(typed_key))
        self.inputs.append(typed_key)
        shortcut = ''.join(self.inputs)
        self.draw_grid_containing_shortcut(shortcut)

        if len(self.inputs) >= self.shortcut_length:
            self.inputs.clear()
            self.canvas.update()  # update the drawn selected cell
            self.canvas.after(50)  # sleep to highlight selected cell
            self.canvas.update()  # show all cells

            print("Shortcut: " + shortcut)
            destination_grid_item = self.get_grid_item_with_shortcut(shortcut)
            if destination_grid_item is not None:
                pyautogui.moveTo(destination_grid_item.center_x, destination_grid_item.center_y)  # should be able to do with pynput
            else:
                print("Shortcut did not match any grid items")
            return True # return true indicating we need to click

    def backspace(self):
        if len(self.inputs) > 0:
            self.inputs.pop()
            command = ''.join(self.inputs)
            self.draw_grid_containing_shortcut(command)

    # clear typed shortcuts in all grids for when switching resolutions
    def clear_input(self):
        self.inputs.clear()
        self.draw_grid_containing_shortcut("")

