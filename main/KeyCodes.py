# keys used to denote a specific box
shortcut_keys = ['Q', 'W', 'E', 'R', 'T', 'A', 'S', 'D', 'F', 'G', 'Z', 'X', 'C', 'V', 'B']
# shortcut_keys = ['q', 'w', 'e', 'r', 't', 'a', 's', 'd', 'f', 'g', 'z', 'x', 'c', 'v', 'b']
base = len(shortcut_keys)


def number_to_base(number):
    if number == 0:
        return [0] * base
    digits = []
    while number:
        digits.append(int(number % base))
        number //= base
    return digits[::-1]


def get_key_code(number):
    result = number_to_base(number)
    result = map(lambda x: str(shortcut_keys[x]), result)
    result = ''.join(result)
    return result


def get_minimal_length(max_value):
    """
    returns the minimal amount of digits at which we can start converting to numbers
    for example: with 44 its 15, because when starting converting counting from 15 we get 2 chars for each shortcut
    we can't have less than 2: we can't have 'w' as a shortcut if another shortcut is 'wa'
    """
    max_length = len(get_key_code(max_value))
    for x in range(0, max_value):
        if len(get_key_code(x)) is max_length:
            return x
