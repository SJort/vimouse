"""
resembles one rectangle on a grid
size: the height and width of the box
code: an unique keycombination which activates this certain GridItem
x and y: the exact pixel coordinates on the screen
column and row: the coordinates within the grid
"""


class GridItem:
    def __init__(self, size, x, y, code):
        self.size = size
        self.x = x
        self.y = y
        self.center_x = x + int(size / 2)
        self.center_y = y + int(size / 2)
        self.column = int(self.x / size)
        self.row = int(self.y / size)
        self.shortcut = code
