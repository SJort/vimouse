from tkinter import *

import pyautogui

from main.Config import *
from main.GridManager import GridManager
from main.KeyCodes import shortcut_keys
from main.ShortcutHandler import ShortcutHandler
from main.Util import *


class Window:

    def __init__(self):
        self.root = Tk()
        self.root.overrideredirect(True)  # disable icon
        self.root.geometry(f"{window_width}x{window_height}+0+0")

        if is_on_windows():
            self.root.attributes('-transparentcolor', self.root['bg'])  # only works on windows
        else:
            self.root.wait_visibility(self.root)  # does not work without
            # self.root.wm_attributes('-alpha', 0.5) # seems to do the same as .attributes
            self.root.attributes("-alpha", 0.5)
        self.grid_manager = GridManager(self.root)  # another class to handle everything on multiple grids for precision

        self.showing = True
        self.set_visibility(self.showing)

        # key() function for keys when window is active
        self.root.bind("<Key>", self.key)

        # always on shortcuts
        self.handler = ShortcutHandler()
        self.handler.add_shortcut(toggle_shortcut_keys, self.toggle_visibility)  # toggle grid

    def toggle_visibility(self):
        self.showing = not self.showing
        self.set_visibility(self.showing)

    def set_visibility(self, show):
        self.showing = show
        if self.showing:
            print("Showing window.")
            self.root.tkraise()
            self.root.deiconify()  # needed for window to show
            self.root.focus_force() # seems to have stopped working (only on linux?)
        else:
            print("Hiding window.")
            self.root.withdraw()

    def start(self):
        print("Starting GUI")
        self.root.mainloop()

    def stop(self):
        print("Stopping GUI.")
        self.root.destroy()

    def key(self, event):
        print(f"Key pressed: {event.keycode}")

        # stop app
        if event.keycode == 27:  # escape
            self.stop()
            # it first just minimized window and cleared inputs
            return

        # manual click
        if event.keycode == 32:  # spacebar
            self.click()
            return

        # delete entered shortcut char
        if event.keycode == 8:  # backspace
            self.grid_manager.active_grid.backspace()
            return

        # increase or decrease amount of boxes: the resolution
        input_char = str(event.char)
        if input_char == "m":
            self.grid_manager.switch(True)
        elif input_char == "M":
            self.grid_manager.switch(False)

        # handle shortcut
        input_upper = str(input_char).upper()  # shortcuts are displayed in upper case
        if input_upper in shortcut_keys:
            if self.grid_manager.active_grid.on_shortcut_input(input_upper):  # returns True when click is needed
                self.click()
            return

        # handle movement
        movement_input = str(event.keysym)
        if movement_input in arrow_keys:
            print("Arrow key movement")
            self.grid_manager.active_grid.move_mouse(Direction(arrow_keys.index(movement_input) + 1), arrow_movement)
        elif movement_input in vim_keys:
            print("Vim key movement")
            self.grid_manager.active_grid.move_mouse(Direction(vim_keys.index(movement_input) + 1), movement)
        elif movement_input in fast_vim_keys:
            print("Fast vim key movement")
            self.grid_manager.active_grid.move_mouse(Direction(fast_vim_keys.index(movement_input) + 1), fast_movement)
        return

    def click(self):
        print("Clicking at", pyautogui.position())
        self.set_visibility(False)
        pyautogui.click()
        self.grid_manager.active_grid.draw_grid_containing_shortcut("")


window = Window()
window.start()
