from pynput import keyboard

"""
holding shortcut key down does not work: have to press again for another
"""


class Shortcut:
    def __init__(self, combination, listener):
        self.combination = combination
        self.listener = listener

    def call_listener(self):
        self.listener()


class ShortcutHandler:
    def __init__(self):
        self.listener = keyboard.Listener(on_press=self.on_press, on_release=self.on_release)
        self.listener.start()
        self.match = False
        self.pressed_shortcut_keys = []
        self.shortcuts = []

    """
    Combination: a list with keys to listen to, use 'Key.cmd' etc for special keys and a string for normal keys like 'a'
    Listener: a function which should be called when shortcut is pressed
    Idea: pass optional **kwargs which should be given with the function
    """

    def add_shortcut(self, combination, listener):
        shortcut = Shortcut(combination, listener)
        self.shortcuts.append(shortcut)

    def on_press(self, key):
        to_add = key

        if hasattr(key, "char"):
            to_add = key.char  # append the char instead of the key

        if to_add not in self.pressed_shortcut_keys:
            self.pressed_shortcut_keys.append(to_add)

        for shortcut in self.shortcuts:
            print(f"Checking {shortcut.combination} with defined {self.pressed_shortcut_keys}")
            if shortcut.combination == self.pressed_shortcut_keys:
                self.match = True
        print(f"Pressed {key}, now {self.pressed_shortcut_keys}")

    def on_release(self, key):
        if self.match:
            for shortcut in self.shortcuts:
                if shortcut.combination == self.pressed_shortcut_keys:
                    shortcut.call_listener()
        self.pressed_shortcut_keys.clear()
