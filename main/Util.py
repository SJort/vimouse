import os
from math import gcd
from enum import Enum
import screeninfo


class Direction(Enum):
    LEFT = 1
    DOWN = 2
    UP = 3
    RIGHT = 4


# use the index to determine the direction
arrow_keys = ["Left", "Down", "Up", "Right"]
vim_keys = ["h", "j", "k", "l"]
fast_vim_keys = ["H", "J", "K", "L"]


# possible factors of 1920x1080: 1, 2, 3, 4, 5, 6, 8, 10, 12, 15, 20, 24, 30, 40, 60, 120
# https://www.calculatorsoup.com/calculators/math/commonfactors.php
# calculate common factors of 2 numbers
# min: the minimal common factor you want returned
def common_factors(x, y, min):
    result = []
    max_factor = gcd(x, y)

    # find all the factors in which the Greatest Common Division is divisible
    for factor in range(1, max_factor):
        if max_factor % factor == 0:
            if min is not None:
                if factor < min:
                    continue
            result.append(factor)

    print(f"Common factor of {x}x{y} max={max_factor}, result={result}")
    return result


# finds the nearest multiple of x
# nearest_multiple(4, 9) -> 8 because 4*2=8 is closed to 9 than 4*3=12
def nearest_multiple(multiple, x):
    return int(x / multiple) * multiple


def is_on_windows():
    return os.name == "nt"


def get_main_screen_dimensions():
    # get primary screen dimensions as (width, height) tuple. unpack by doing: width, height = get_...
    screens = screeninfo.get_monitors()
    for screen in screens:
        if not screen.is_primary:
            continue
        return screen.width, screen.height
