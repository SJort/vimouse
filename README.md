# Vimouse
Vimouse is an application to control mouse with keyboard.  
It is an overlay which you can call with a shortcut. The overlay devides the screen
in boxes, which each their own code. Type the code to click in the center of the box.
You can also manually select which box to click with Vi-like shortcuts.

## Demo
[YouTube](https://youtu.be/1x108BrTEy4)

## Plan
* Shortcut handler
* Executable creator
* Basic grid window
* Advanced grid window
* Multiple grids
* Mouse for grid
* Argument passing for movement functions

## Create installer
Install cx_Freeze
```shell
pip install cx_Freeze
```
Generate setup.py file
```shell
cxfreeze-quickstart
```
Build installer
```shell
python setup.py bdist_msi
```

# Packages
```shell
pip install pyautogui
```
Installation may have to be done with command line.  
PyCharm may need to invalidate caches to recognize the installed package.

# TODO (for developer)
* Following the mouse movement function, there is a sleep build in of 0.01 s?
* The entered input command activates too late (5)
* I probably should enable precision within selected box



